package com.example.thehu.chicken_farm_management_app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thehu.chicken_farm_management_app.R;
import com.example.thehu.chicken_farm_management_app.database.DB_helper_methods;
import com.example.thehu.chicken_farm_management_app.models.Farm;
import com.example.thehu.chicken_farm_management_app.models.Shed;

import java.util.ArrayList;
import java.util.List;

//Same as adding a farm but for a shed for that farm
public class Add_shed_activity extends AppCompatActivity {
    List<Shed> sheds;
    TextView shedNumber;
    TextView shedText;
    AppCompatSpinner siloCount;
    Button saveShed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Setting layout to add shed layout
        setContentView(R.layout.add_shed_layout);

        sheds = new ArrayList<>();
        //Currently silo count doesn't do anything in the rest of the app, its here for future implementations
        shedText = findViewById(R.id.tvShedText);
        shedNumber = findViewById(R.id.tvShedCount);
        siloCount = findViewById(R.id.spCategorySilos);
        saveShed = findViewById(R.id.bSaveShed);

        Intent intent = getIntent();
        //Gets the number of sheds already and adds one more to it and sets that number to shedNumber
        int numberOfSheds = intent.getIntExtra("numberOfSheds", 0);
        numberOfSheds++;
        shedNumber.setText(String.valueOf(numberOfSheds));


        saveShed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Saving the shed to the DB
                Shed shed = new Shed();
                shed.setName(shedText.getText().toString().trim() + " " + shedNumber.getText().toString().trim());
                shed.setSilo_count(Integer.parseInt(siloCount.getSelectedItem().toString()));

                Intent intent = new Intent();
                intent.putExtra("shed", shed);
                intent.putExtra("containData", true);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }


        });

    }


}






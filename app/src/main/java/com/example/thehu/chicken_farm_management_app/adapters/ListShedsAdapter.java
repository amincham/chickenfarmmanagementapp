package com.example.thehu.chicken_farm_management_app.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.thehu.chicken_farm_management_app.R;
import com.example.thehu.chicken_farm_management_app.models.Farm;
import com.example.thehu.chicken_farm_management_app.models.Shed;
import com.example.thehu.chicken_farm_management_app.viewholders.EmptyViewHolder;
import com.example.thehu.chicken_farm_management_app.viewholders.ListFarmViewHolder;
import com.example.thehu.chicken_farm_management_app.viewholders.ListShedsViewHolder;

import java.util.List;

//Gets all the Sheds for the particular farm in the DB for then to add to the List
public class ListShedsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_FOOTER = 1;
    private static final int VIEW_TYPE_EMPTY = 2;
    Context context;
    List<Shed> list;

    public ListShedsAdapter(Context context, List<Shed> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        if (viewType == VIEW_TYPE_ITEM) {
            View view = inflater.inflate(R.layout.item_farm, parent, false);
            viewHolder = new ListShedsViewHolder(view);
        } else if (viewType == VIEW_TYPE_EMPTY) {
            View view = inflater.inflate(R.layout.layout_empty_view, parent, false);
            viewHolder = new EmptyViewHolder(view);
        }
        return viewHolder;

    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ListShedsViewHolder) {
            ListShedsViewHolder viewHolder = (ListShedsViewHolder) holder;
            viewHolder.Populate(list.get(position), context);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (list.size() == 0)
            return VIEW_TYPE_EMPTY;
        return VIEW_TYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        int size = this.list == null ? 0 : this.list.size();
        if (size == 0)
            return 1;
        return size;
    }

}
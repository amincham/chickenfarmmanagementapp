package com.example.thehu.chicken_farm_management_app.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {
    //The Three databases used in this app
    public static final String FARMS_TABLE = "farms";
    public static final String ID_COLUMN = "id";
    public static final String FARM_NAME = "farm_name";
    public static final String FARM_TYPE = "farm_type";
    public static final String SHED_COUNT = "shed_count";
    public static final String CREATE_FARMS_TABLE = "CREATE TABLE "
            + FARMS_TABLE + "(" + ID_COLUMN + " INTEGER PRIMARY KEY," + FARM_NAME
            + " TEXT, " + FARM_TYPE + " TEXT, " + SHED_COUNT + " TEXT)";

    public static final String SHEDS_TABLE = "sheds";
    public static final String ID_COLUMN_TWO = "id";
    public static final String DAY_NUMBER = "day_number";
    public static final String TOTAL_FEED_USED = "total_feed_used";
    public static final String TOTAL_FEED = "total_feed";
    public static final String SHED_NAME = "shed_name";
    public static final String STARTED_RUN = "started_run";
    public static final String NUMBER_OF_BIRDS = "number_of_birds";
    public static final String SILO_COUNT = "silo_count";
    public static final String FORM_ID = "form_id";
    public static final String SHED_DATE = "shed_date";
    public static final String CREATE_SHEDS_TABLE = "CREATE TABLE "
            + SHEDS_TABLE + "(" + ID_COLUMN_TWO + " INTEGER PRIMARY KEY," + SHED_NAME
            + " TEXT, " + FORM_ID + " TEXT, " + DAY_NUMBER + " TEXT, " + TOTAL_FEED_USED + " TEXT, " + TOTAL_FEED + " TEXT, " + STARTED_RUN + " TEXT, " + NUMBER_OF_BIRDS + " TEXT, " + SHED_DATE + " TEXT, " + SILO_COUNT + " TEXT)";
    private static final String DATABASE_NAME = "farmsDB";
    private static final int DATABASE_VERSION = 1;
    private static DataBaseHelper instance;

    public static final String SINGLE_DAY_TABLE = "single_day";
    public static final String DAY_COUNT_SHED = "day_count_day";
    public static final String NUM_CHICKENS_DAY = "num_chickens_day";
    public static final String FEED_COUNT_DAY = "feed_count_day";
    public static final String SHED_ID = "shed_id";
    public static final String DATE = "date";
    public static final String CREATE_SINGLE_SHEDS_TABLE = "CREATE TABLE "
            + SINGLE_DAY_TABLE + "(" + SHED_ID + " INTEGER PRIMARY KEY," + DAY_COUNT_SHED
            + " TEXT, " + DATE + " TEXT," + NUM_CHICKENS_DAY + " TEXT, " + FEED_COUNT_DAY + " TEXT)";


    private DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DataBaseHelper getHelper(Context context) {
        if (instance == null)
            instance = new DataBaseHelper(context);
        return instance;
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Creating the databases
        db.execSQL(CREATE_SHEDS_TABLE);
        db.execSQL(CREATE_FARMS_TABLE);
        db.execSQL(CREATE_SINGLE_SHEDS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}

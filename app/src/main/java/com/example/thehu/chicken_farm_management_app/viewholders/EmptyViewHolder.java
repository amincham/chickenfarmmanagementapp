package com.example.thehu.chicken_farm_management_app.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

//ViewHolder for no Farms
public class EmptyViewHolder extends RecyclerView.ViewHolder {
    public EmptyViewHolder(View itemView) {
        super(itemView);
    }
}


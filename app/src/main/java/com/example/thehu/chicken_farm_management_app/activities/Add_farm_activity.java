package com.example.thehu.chicken_farm_management_app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;

import com.example.thehu.chicken_farm_management_app.database.DB_helper_methods;
import com.example.thehu.chicken_farm_management_app.database.DataBaseHelper;
import com.example.thehu.chicken_farm_management_app.models.Farm;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thehu.chicken_farm_management_app.R;
import com.example.thehu.chicken_farm_management_app.models.Shed;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//Class for adding each Farm for the user, saves it to the DB
public class Add_farm_activity extends AppCompatActivity {
    //Variables
    List<Farm> farms;
    TextInputEditText farmName;
    AppCompatSpinner farmType;
    TextView shedNumber;
    TextInputLayout farmNameWrapper;
    private List<Shed> shedList;
    private int numOfSheds = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Setting the view to the add farm layout
        setContentView(R.layout.add_farm_layout);
        //Setting the Arrays for the farms the the ShedList
        farms = new ArrayList<>();
        shedList = new ArrayList<>();

        //Setting the variables to the text boxes
        farmName = findViewById(R.id.edtFarmName);
        farmType = findViewById(R.id.tvFeedLeft);
        shedNumber = findViewById(R.id.tvShedCount);
        farmNameWrapper = findViewById(R.id.tilFarmNameWrapper);

        findViewById(R.id.bsaveFarm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validation()) {
                    DB_helper_methods farmMethods = new DB_helper_methods(Add_farm_activity.this);
                    //Setting all of the data for the particular farm
                    Farm farm = new Farm();
                    farm.setName(farmName.getText().toString().trim());
                    farm.setType(farmType.getSelectedItem().toString());
                    farm.setNoOfSheds(Integer.parseInt(shedNumber.getText().toString()));
                    //This id is to help the app know what farm and what shed is which, for data getting later on
                    long formId = farmMethods.saveFarm(farm);

                    for (Shed shed : shedList) {
                        shed.setDayNumber(0);
                        shed.setFormId(formId);
                        farmMethods.saveShed(shed)
                        ;
                    }

                    //Toast to tell the user that the farm has been saved
                    Toast.makeText(Add_farm_activity.this, "Farm has been saved successfully!", Toast.LENGTH_LONG).show();

                    //Make this farm list refresh so the user can see the new farm added
                    Intent intent = new Intent();
                    intent.putExtra("updateList", true);
                    setResult(Activity.RESULT_OK, intent);
                    finish();

                }

            }

        });
        //Setting a FAB for adding another shed for the farm
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Start the intent to add a shed for the farm
                Intent intent = new Intent(Add_farm_activity.this, Add_shed_activity.class);
                intent.putExtra("numberOfSheds", numOfSheds);
                startActivityForResult(intent, 60);
            }
        });
    }

    //Validation function for adding a farm
    private boolean validation() {

        boolean isValid = true;
        //Check if farm name is empty
        if (farmName.getText().toString().trim().isEmpty()) {
            farmNameWrapper.setError(getString(R.string.msg_cannot_empty));
            isValid = false;
        } else {
            farmNameWrapper.setError(null);
        }
        //Check if user has added a shed or not
        if (shedNumber.getText().toString().trim().equalsIgnoreCase("0")) {
            Toast.makeText(this, "Cannot have zero sheds!", Toast.LENGTH_LONG).show();
            isValid = false;

        }
        return isValid;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Added the shed for that farm and increases the shednumber
        if (requestCode == 60 && resultCode == Activity.RESULT_OK && data.getBooleanExtra("containData", false)) {

            Shed shed = (Shed) data.getSerializableExtra("shed");
            shedList.add(shed);
            numOfSheds++;
            shedNumber.setText(String.valueOf(numOfSheds));

        }
    }
}

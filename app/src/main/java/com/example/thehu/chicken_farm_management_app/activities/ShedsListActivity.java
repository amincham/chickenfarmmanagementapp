package com.example.thehu.chicken_farm_management_app.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.thehu.chicken_farm_management_app.R;
import com.example.thehu.chicken_farm_management_app.adapters.ListShedsAdapter;
import com.example.thehu.chicken_farm_management_app.database.DB_helper_methods;
import com.example.thehu.chicken_farm_management_app.models.Shed;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

//Shows the List of Sheds in the Particular Farm
public class ShedsListActivity extends AppCompatActivity {

    private RecyclerView rvSheds;
    private long formId;
    private List<Shed> sheds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sheds_list);
        rvSheds = findViewById(R.id.rvFarm);

        Intent intent = getIntent();
        formId = intent.getLongExtra("farmId", 0);
    }

    private void updateList() {

        DB_helper_methods methods = new DB_helper_methods(this);

        sheds = methods.getShed(formId);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvSheds.setLayoutManager(linearLayoutManager);
        ListShedsAdapter listShedsAdapter = new ListShedsAdapter(this, sheds);
        rvSheds.setAdapter(listShedsAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        updateList();
    }
}

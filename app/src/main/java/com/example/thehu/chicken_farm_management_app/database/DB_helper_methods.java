package com.example.thehu.chicken_farm_management_app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import com.example.thehu.chicken_farm_management_app.models.DailyShed;
import com.example.thehu.chicken_farm_management_app.models.Farm;
import com.example.thehu.chicken_farm_management_app.models.Shed;

import java.util.ArrayList;
import java.util.List;

//Class which updates, saves, deletes all databases
//All implementation same as the next
public class DB_helper_methods extends DB_Initiater {

    private static final String WHERE_ID_EQUALS = DataBaseHelper.ID_COLUMN
            + " =?";

    public DB_helper_methods(Context context) {
        super(context);
    }

    public long saveFarm(Farm marker_data) {
        //Gets all data and saves it to the DB for that Farm
        ContentValues values = new ContentValues();
        values.put(DataBaseHelper.FARM_NAME, marker_data.getName());
        values.put(DataBaseHelper.FARM_TYPE, marker_data.getType());
        values.put(DataBaseHelper.SHED_COUNT, marker_data.getNoOfSheds());


        return database
                .insert(DataBaseHelper.FARMS_TABLE, null, values);
    }

    public long updateFarm(Farm marker_data) {
        //Same as Save but instead of inserting new DB it updates it
        ContentValues values = new ContentValues();
        values.put(DataBaseHelper.FARM_NAME, marker_data.getName());
        values.put(DataBaseHelper.FARM_TYPE, marker_data.getType());
        values.put(DataBaseHelper.SHED_COUNT, marker_data.getNoOfSheds());

        long result = database.update(DataBaseHelper.FARMS_TABLE,
                values, WHERE_ID_EQUALS,
                new String[]{String.valueOf(marker_data.getId())});
        Log.d("Update Result:", "=" + result);
        return result;

    }


    public List<Farm> getFarms() {
        List<Farm> plang_list = new ArrayList<>();

        //Goes through the db and gets the data
        Cursor cursor = database.query(DataBaseHelper.FARMS_TABLE,
                new String[]{DataBaseHelper.ID_COLUMN, DataBaseHelper.FARM_NAME,
                        DataBaseHelper.FARM_TYPE, DataBaseHelper.SHED_COUNT,
                }, null,
                null, null, null, null);

        while (cursor.moveToNext()) {
            Farm plang_bin = new Farm();
            plang_bin.setId(cursor.getLong(0));
            plang_bin.setName(cursor.getString(1));
            plang_bin.setType(cursor.getString(2));
            plang_bin.setNoOfSheds(cursor.getInt(3));
            plang_list.add(plang_bin);
        }
        return plang_list;
    }

    //Simple deletion of the DB
    public void deleteFarm(Long id, Context context) {


        long result = database.delete(DataBaseHelper.FARMS_TABLE,
                WHERE_ID_EQUALS,
                new String[]{String.valueOf(id)});
        Log.d("Delete Result:", "=" + result);

        if (result == 1) {

            // Message
            Toast.makeText(context, "Farm is deleted successfully", Toast.LENGTH_LONG).show();
        }

    }

    public long saveShed(Shed marker_data) {

        ContentValues values = new ContentValues();
        values.put(DataBaseHelper.SHED_NAME, marker_data.getName());
        values.put(DataBaseHelper.NUMBER_OF_BIRDS, marker_data.getNum_birds());
        values.put(DataBaseHelper.SILO_COUNT, marker_data.getSilo_count());
        values.put(DataBaseHelper.FORM_ID, marker_data.getFormId());
        values.put(DataBaseHelper.SHED_DATE, marker_data.getDate());


        return database
                .insert(DataBaseHelper.SHEDS_TABLE, null, values);
    }

    public long updateShed(Shed marker_data) {
        ContentValues values = new ContentValues();
        values.put(DataBaseHelper.SHED_NAME, marker_data.getName());
        values.put(DataBaseHelper.NUMBER_OF_BIRDS, marker_data.getNum_birds());
        values.put(DataBaseHelper.SILO_COUNT, marker_data.getSilo_count());
        values.put(DataBaseHelper.TOTAL_FEED, marker_data.getFeed_count());
        values.put(DataBaseHelper.STARTED_RUN, marker_data.getStartRun());
        values.put(DataBaseHelper.SHED_DATE, marker_data.getDate());
        values.put(DataBaseHelper.DAY_NUMBER, marker_data.getDayNumber());
        //values.put(DataBaseHelper.TOTAL_FEED_USED,marker_data.)

        long result = database.update(DataBaseHelper.SHEDS_TABLE,
                values, WHERE_ID_EQUALS,
                new String[]{String.valueOf(marker_data.getId())});
        Log.d("Update Result:", "=" + result);
        return result;

    }


    public List<Shed> getShed(long farmId) {
        List<Shed> plang_list = new ArrayList<>();


        Cursor cursor = database.query(DataBaseHelper.SHEDS_TABLE,
                new String[]{DataBaseHelper.ID_COLUMN, DataBaseHelper.SHED_NAME,
                        DataBaseHelper.NUMBER_OF_BIRDS, DataBaseHelper.SILO_COUNT, DataBaseHelper.FORM_ID, DataBaseHelper.TOTAL_FEED, DataBaseHelper.STARTED_RUN, DataBaseHelper.DAY_NUMBER, DataBaseHelper.TOTAL_FEED_USED, DataBaseHelper.SHED_DATE
                }, DataBaseHelper.FORM_ID + "='" + farmId + "'",
                null, null, null, null);

        while (cursor.moveToNext()) {
            Shed plang_bin = new Shed();
            plang_bin.setId(cursor.getLong(0));
            plang_bin.setName(cursor.getString(1));
            plang_bin.setNum_birds(cursor.getLong(2));
            plang_bin.setSilo_count(cursor.getLong(3));
            plang_bin.setFormId(cursor.getLong(4));
            plang_bin.setFeed_count(cursor.getDouble(5));
            plang_bin.setStartRun(cursor.getInt(6));
            plang_bin.setDayNumber(cursor.getInt(7));
            plang_bin.setTotal_feed_used(cursor.getDouble(8));
            plang_bin.setDate(cursor.getString(9));
            plang_list.add(plang_bin);
        }
        return plang_list;
    }


    public void deleteShed(Long id, Context context) {


        long result = database.delete(DataBaseHelper.SHEDS_TABLE,
                WHERE_ID_EQUALS,
                new String[]{String.valueOf(id)});
        Log.d("Delete Result:", "=" + result);

        if (result == 1) {

            // Message
            Toast.makeText(context, "Shed is deleted successfully", Toast.LENGTH_LONG).show();
        }

    }

    public long saveDailyShed(DailyShed marker_data) {

        ContentValues values = new ContentValues();
        values.put(DataBaseHelper.DAY_COUNT_SHED, marker_data.getDay_Count_Shed());
        values.put(DataBaseHelper.NUM_CHICKENS_DAY, marker_data.getNum_Chickens_Day());
        values.put(DataBaseHelper.DATE, marker_data.getDate());
        values.put(DataBaseHelper.SHED_ID, marker_data.getShed_ID());
        values.put(DataBaseHelper.FEED_COUNT_DAY, marker_data.getFeed_Count_Shed());


        return database
                .insert(DataBaseHelper.SINGLE_DAY_TABLE, null, values);
    }

    public long updateDailyShed(DailyShed marker_data) {
        ContentValues values = new ContentValues();
        values.put(DataBaseHelper.DAY_COUNT_SHED, marker_data.getDay_Count_Shed());
        values.put(DataBaseHelper.NUM_CHICKENS_DAY, marker_data.getNum_Chickens_Day());
        values.put(DataBaseHelper.DATE, marker_data.getDate());
        values.put(DataBaseHelper.SHED_ID, marker_data.getShed_ID());
        values.put(DataBaseHelper.FEED_COUNT_DAY, marker_data.getFeed_Count_Shed());

        long result = database.update(DataBaseHelper.SINGLE_DAY_TABLE,
                values, WHERE_ID_EQUALS,
                new String[]{String.valueOf(marker_data.getShed_ID())});
        Log.d("Update Result:", "=" + result);
        return result;

    }


    public List<DailyShed> getDailyShed(long shedID) {
        List<DailyShed> plang_list = new ArrayList<>();


        Cursor cursor = database.query(DataBaseHelper.SINGLE_DAY_TABLE,
                new String[]{DataBaseHelper.SHED_ID, DataBaseHelper.DAY_COUNT_SHED,
                        DataBaseHelper.FEED_COUNT_DAY, DataBaseHelper.NUM_CHICKENS_DAY, DataBaseHelper.DATE
                }, DataBaseHelper.SHED_ID + "='" + shedID + "'",
                null, null, null, null);

        while (cursor.moveToNext()) {
            DailyShed plang_bin = new DailyShed();
            plang_bin.setShed_ID(cursor.getLong(0));
            plang_bin.setDay_Count_Shed(cursor.getDouble(1));
            plang_bin.setFeed_Count_Shed(cursor.getDouble(2));
            plang_bin.setNum_Chickens_Day(cursor.getDouble(3));
            plang_bin.setDate(cursor.getString(4));
            plang_list.add(plang_bin);
        }
        return plang_list;
    }


    public void deleteDailyShed(Long id, Context context) {


        long result = database.delete(DataBaseHelper.SINGLE_DAY_TABLE,
                DataBaseHelper.SHED_ID + "='" + id + "'", null);
        Log.d("Delete Result:", "=" + result);

        if (result == 1) {

            // Message
            Toast.makeText(context, "Daily Shed is deleted successfully", Toast.LENGTH_LONG).show();
        }

    }
}

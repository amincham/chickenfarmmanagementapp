package com.example.thehu.chicken_farm_management_app.models;

import java.io.Serializable;

//Getters and Setters for Shed
public class Shed implements Serializable {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getSilo_count() {
        return silo_count;
    }

    public void setSilo_count(long silo_count) {
        this.silo_count = silo_count;
    }

    public long getNum_birds() {
        return num_birds;
    }

    public void setNum_birds(long num_birds) {
        this.num_birds = num_birds;
    }

    private String name;
    private long id;
    private long silo_count;
    private long num_birds;
    private long formId;
    private double feed_count;
    private double total_feed_used;
    private int startRun;
    private int dayNumber;
    private String date;

    public int getStartRun() {
        return startRun;
    }

    public void setStartRun(int startRun) {
        this.startRun = startRun;
    }

    public long getFormId() {
        return formId;
    }

    public void setFormId(long formId) {
        this.formId = formId;
    }

    public double getFeed_count() {
        return feed_count;
    }

    public void setFeed_count(double feed_count) {
        this.feed_count = feed_count;
    }

    public double getTotal_feed_used() {
        return total_feed_used;
    }

    public void setTotal_feed_used(double total_feed_used) {
        this.total_feed_used = total_feed_used;
    }

    public int getDayNumber() {
        return dayNumber;
    }

    public void setDayNumber(int dayNumber) {
        this.dayNumber = dayNumber;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}

package com.example.thehu.chicken_farm_management_app.viewholders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.thehu.chicken_farm_management_app.R;
import com.example.thehu.chicken_farm_management_app.activities.ShedDetailActivity;
import com.example.thehu.chicken_farm_management_app.activities.ShedsListActivity;
import com.example.thehu.chicken_farm_management_app.models.Shed;

public class ListShedsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    //Populating the View with all Shed for the particular Farm in DB
    private View view;
    private Context context;
    private Shed shed;
    private TextView farmName;
    private TextView farmType;
    private TextView numberOfSheds;

    public ListShedsViewHolder(View itemView) {
        super(itemView);
        this.view = itemView;
        farmName = itemView.findViewById(R.id.tvFarmName);
        farmType = itemView.findViewById(R.id.tvFarmType);
        numberOfSheds = itemView.findViewById(R.id.tvFarmSheds);
        view.setOnClickListener(this);
    }

    public void Populate(Shed data, Context context) {
        shed = data;
        farmName.setText(shed.getName());
        farmType.setText(String.valueOf(shed.getNum_birds()) + " Chickens");
        numberOfSheds.setText(String.valueOf(shed.getFeed_count()) + " Tonne Left");
        this.context = context;

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(context, ShedDetailActivity.class);
        intent.putExtra("shed", shed);
        context.startActivity(intent);
    }
}

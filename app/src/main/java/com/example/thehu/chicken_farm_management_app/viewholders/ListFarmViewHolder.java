package com.example.thehu.chicken_farm_management_app.viewholders;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;

import com.example.thehu.chicken_farm_management_app.R;

import android.view.TextureView;
import android.view.View;
import android.widget.TextView;

import com.example.thehu.chicken_farm_management_app.activities.FarmActivity;
import com.example.thehu.chicken_farm_management_app.activities.ShedsListActivity;
import com.example.thehu.chicken_farm_management_app.models.Farm;

public class ListFarmViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    //Populating the View with all Farms in DB
    private View view;
    private Context context;
    private Farm farm;
    private TextView farmName;
    private TextView farmType;
    private TextView numberOfSheds;

    public ListFarmViewHolder(View itemView) {
        super(itemView);
        this.view = itemView;
        farmName = itemView.findViewById(R.id.tvFarmName);
        farmType = itemView.findViewById(R.id.tvFarmType);
        numberOfSheds = itemView.findViewById(R.id.tvFarmSheds);
        view.setOnClickListener(this);
    }

    public void Populate(Farm data, Context context) {
        farm = data;
        farmName.setText("Name: " + farm.getName());
        farmType.setText("Type: " + farm.getType());
        numberOfSheds.setText(String.valueOf(farm.getNoOfSheds()) + " Shed/s");
        this.context = context;

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(context, ShedsListActivity.class);
        intent.putExtra("farmId", farm.getId());
        context.startActivity(intent);
    }
}

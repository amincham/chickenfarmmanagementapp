package com.example.thehu.chicken_farm_management_app.models;

//Getters and Setters for Farms
public class Farm {
    private String name;
    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNoOfSheds() {
        return noOfSheds;
    }

    public void setNoOfSheds(int noOfSheds) {
        this.noOfSheds = noOfSheds;
    }

    private String type;
    private int noOfSheds;


}

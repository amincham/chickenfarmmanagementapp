package com.example.thehu.chicken_farm_management_app.models;

//Getters and Setters for Daily Shed
public class DailyShed {
    private Double day_Count_Shed;

    public Double getDay_Count_Shed() {
        return day_Count_Shed;
    }

    public void setDay_Count_Shed(Double day_Count_Shed) {
        this.day_Count_Shed = day_Count_Shed;
    }

    public Double getNum_Chickens_Day() {
        return num_Chickens_Day;
    }

    public void setNum_Chickens_Day(Double num_Chickens_Day) {
        this.num_Chickens_Day = num_Chickens_Day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getFeed_Count_Shed() {
        return feed_Count_Shed;
    }

    public void setFeed_Count_Shed(Double feed_Count_Shed) {
        this.feed_Count_Shed = feed_Count_Shed;
    }

    public Long getShed_ID() {
        return shed_ID;
    }

    public void setShed_ID(Long shed_ID) {
        this.shed_ID = shed_ID;
    }

    private Double num_Chickens_Day;
    private String date;
    private Double feed_Count_Shed;
    private Long shed_ID;
}

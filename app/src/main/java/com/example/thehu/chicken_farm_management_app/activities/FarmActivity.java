package com.example.thehu.chicken_farm_management_app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.thehu.chicken_farm_management_app.adapters.ListFarmsAdapter;
import com.example.thehu.chicken_farm_management_app.database.DB_helper_methods;
import com.example.thehu.chicken_farm_management_app.models.Farm;

import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.thehu.chicken_farm_management_app.R;

import java.util.ArrayList;
import java.util.List;

//Main  Starting page for the app
public class FarmActivity extends AppCompatActivity {

    RecyclerView rvFarm;
    List<Farm> farms;

    @Override
    //Get all the farms in the DB and display them
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.farmlayout);
        rvFarm = findViewById(R.id.rvFarm);

        updateList();


        //FAB for adding new Farms
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivityForResult(new Intent(FarmActivity.this, Add_farm_activity.class), 60);
            }
        });
    }

    private void updateList() {
        //Adds all farms which are in the db to the List
        DB_helper_methods methods = new DB_helper_methods(this);
        farms = methods.getFarms();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvFarm.setLayoutManager(linearLayoutManager);
        ListFarmsAdapter listFarmsAdapter = new ListFarmsAdapter(this, farms);
        rvFarm.setAdapter(listFarmsAdapter);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Refreshes the List
        if (requestCode == 60 && resultCode == Activity.RESULT_OK) {
            if (data.getBooleanExtra("updateList", false)) {
                updateList();
            }
        }
    }
}

package com.example.thehu.chicken_farm_management_app.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.thehu.chicken_farm_management_app.R;
import com.example.thehu.chicken_farm_management_app.database.DB_helper_methods;
import com.example.thehu.chicken_farm_management_app.models.DailyShed;
import com.example.thehu.chicken_farm_management_app.models.Shed;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

//Class where most of the app is all about. Calculates everything to do with the particular Shed
public class ShedDetailActivity extends AppCompatActivity {
    //Declaring all variables
    TextView numberOfDayCount, numberOfChickens, feedLeft, updateFCR;
    TextInputLayout mortUpdateWrapper, feedUpdateWrapper, inputLayout, inputLayout2, inputLayout3;
    TextInputEditText mortUpdate, feedUpdate;
    Button siloChange, calculateFCR, updateDay;
    TextInputEditText input, input2, input3;
    int dayNumber = 0;
    Shed shed;
    DB_helper_methods methods;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Sets View
        setContentView(R.layout.layout_shed_details);
        //Setting all variables
        numberOfDayCount = findViewById(R.id.tvNumberofDaysCount);
        numberOfChickens = findViewById(R.id.tvNumberofChickenCount);
        feedLeft = findViewById(R.id.tvFeedLeft);
        mortUpdateWrapper = findViewById(R.id.tilMortUpdateWrapper);
        feedUpdateWrapper = findViewById(R.id.tilFeedUpdateWrapper);
        mortUpdate = findViewById(R.id.edtMortUpdateUser);
        feedUpdate = findViewById(R.id.edtFeedUpdateUser);
        siloChange = findViewById(R.id.bUpdateSilos);
        calculateFCR = findViewById(R.id.bCalculateFCR);
        updateDay = findViewById(R.id.bUpdateShedDetails);
        updateFCR = findViewById(R.id.tvUpdateFCR);

        Intent intent = getIntent();

        shed = (Shed) intent.getSerializableExtra("shed");


        //If the user has not started the run, user cannot type anything into the fields
        if (shed.getStartRun() == 0) {
            mortUpdate.setFocusable(false);
            feedUpdate.setFocusable(false);

        } else {
            //Checking the Date and comparing with the saved Date in the DB, if its a new day the user can type new details in the field
            //If not user cannot
            //The user can 'catch up' on days where he/she has not entered details either
            Date date = getDate(shed.getDate());


            TimeIgnoringComparator timeIgnoringComparator = new TimeIgnoringComparator();


            dayNumber = timeIgnoringComparator.compare(new Date(), date);
            Log.i("Date comparison", "" + dayNumber);

            if (shed.getDayNumber() > 42) {
                Toast.makeText(ShedDetailActivity.this, "Need to End Run.", Toast.LENGTH_SHORT).show();
            } else {
                numberOfDayCount.setText(String.valueOf(shed.getDayNumber()));
            }

        }


        //FCR - Feed Conversion Rate
        //Calculation to show a number to show how much feed is being eaten per kilo of weight gained
        calculateFCR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shed.getStartRun() != 0) {

                    final AlertDialog.Builder builder = new AlertDialog.Builder(ShedDetailActivity.this);
                    builder.setTitle("Calculate FCR");
                    builder.setCancelable(false);

                    //Inflating the custom view
                    View viewInflated = LayoutInflater.from(ShedDetailActivity.this).inflate(R.layout.layout_dialog, null);

                    // Set up the input
                    input = viewInflated.findViewById(R.id.edtStartChicken);
                    input2 = viewInflated.findViewById(R.id.edtStartFeed);
                    inputLayout = viewInflated.findViewById(R.id.tilstartChickenWrapper);
                    inputLayout2 = viewInflated.findViewById(R.id.tilstartFeedWrapper);

                    input.setHint("Enter Average Weight");
                    //Because only one input is needed we hide the other
                    inputLayout2.setVisibility(View.INVISIBLE);

                    // Specify the type of input expected; this
                    builder.setView(viewInflated);

                    // Set up the buttons
                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if (updateFeedValidation()) {
                                //if validation is true get all the data needed from the db and do the calculation
                                methods = new DB_helper_methods(ShedDetailActivity.this);
                                List<DailyShed> list = methods.getDailyShed(shed.getId());

                                if (list.size() > 0) {
                                    double totalFeedUsed = 0.0;

                                    for (DailyShed dailyShed : list) {
                                        totalFeedUsed += dailyShed.getFeed_Count_Shed();
                                    }

                                    double demon = shed.getNum_birds() * Double.valueOf(input.getText().toString());
                                    if (demon > 0.0) {
                                        double FCR = totalFeedUsed / demon;
                                        updateFCR.setText("Calculate FCR: " + FCR);
                                    }
                                }
                                dialog.dismiss();
                            } else {
                                //If value is empty, display Toast
                                Toast.makeText(ShedDetailActivity.this, "Value cannot be empty", Toast.LENGTH_SHORT).show();
                            }
                        }


                    });

                    //Show the dialog
                    builder.show();
                } else {
                    //If the Run has not been started display a Toast
                    Toast.makeText(ShedDetailActivity.this, "Need to Start Run First", Toast.LENGTH_LONG).show();
                }
            }
        });
        //For updating the Silo amount if new feed has arrived on the farm
        siloChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shed.getStartRun() != 0) {

                    final AlertDialog.Builder builder = new AlertDialog.Builder(ShedDetailActivity.this);
                    builder.setTitle("Update Feed ");
                    builder.setCancelable(false);

                    //Inflating the custom view
                    View viewInflated = LayoutInflater.from(ShedDetailActivity.this).inflate(R.layout.layout_dialog, null);

                    // Set up the input
                    input = viewInflated.findViewById(R.id.edtStartChicken);
                    input2 = viewInflated.findViewById(R.id.edtStartFeed);
                    inputLayout = viewInflated.findViewById(R.id.tilstartChickenWrapper);
                    inputLayout2 = viewInflated.findViewById(R.id.tilstartFeedWrapper);
                    //Because only one input is needed, hide the other
                    input.setHint("Enter Feed (000's)");

                    inputLayout2.setVisibility(View.INVISIBLE);

                    // Specify the type of input expected; this
                    builder.setView(viewInflated);

                    // Set up the buttons
                    builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            if (updateFeedValidation()) {

                                //if validation is true, update the feed count
                                methods = new DB_helper_methods(ShedDetailActivity.this);
                                shed.setFeed_count(shed.getFeed_count() + Double.valueOf(input.getText().toString()));
                                feedLeft.setText("Feed Left: " + String.valueOf(shed.getFeed_count()) + " Tonne");
                                shed.setStartRun(1);
                                methods.updateShed(shed);
                                dialog.dismiss();
                            } else {
                                //Value cannot be empty
                                Toast.makeText(ShedDetailActivity.this, "Value cannot be empty", Toast.LENGTH_SHORT).show();
                            }
                        }


                    });


                    builder.show();
                } else {
                    //Need to start run first before clicking button
                    Toast.makeText(ShedDetailActivity.this, "Need to Start Run First", Toast.LENGTH_LONG).show();
                }
            }
        });

        numberOfChickens.setText(String.valueOf(shed.getNum_birds()));
        feedLeft.setText("Feed Left: " + String.valueOf(shed.getFeed_count()) + " Tonne");
        //Updating the day when the user types in data for that day
        updateDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Need to start run first
                if (shed.getStartRun() != 0) {
                    //Validation needs to be true, and daynumber needs to be more than zero due to no data will be available on day zero
                    if (validation2() && dayNumber > 0) {
                        long remainChickens;
                        //Calculate remaining chickens
                        remainChickens = shed.getNum_birds() - Long.valueOf(mortUpdate.getText().toString());
                        //Calculate the feed used
                        double feedUsed = (Double.valueOf(feedUpdate.getText().toString())) * shed.getNum_birds();
                        //Set the remaining chickens in db
                        shed.setNum_birds(remainChickens);
                        //Set text for number of chickens left
                        numberOfChickens.setText(String.valueOf(shed.getNum_birds()));

                        //Updating feedCount
                        if (shed.getFeed_count() > 0) {
                            Double totalFeed = shed.getFeed_count() - feedUsed;
                            shed.setFeed_count(totalFeed);
                            feedLeft.setText(String.valueOf(shed.getFeed_count()));
                        }
                        //Updating the time to calculate for next update availability
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String strDate = sdf.format(new Date());
                        //Setting the day number to +1
                        shed.setDayNumber(shed.getDayNumber() + 1);
                        numberOfDayCount.setText(String.valueOf(shed.getDayNumber()));

                        //Sets how many more times the user can enter another input, allows catch up of data
                        dayNumber--;
                        //Setting date, and update the shed
                        shed.setDate(strDate);
                        DB_helper_methods methods = new DB_helper_methods(ShedDetailActivity.this);
                        methods.updateShed(shed);

                        DailyShed dailyShed = new DailyShed();

                        dailyShed.setDate(strDate);
                        dailyShed.setNum_Chickens_Day((double) remainChickens);
                        dailyShed.setFeed_Count_Shed(feedUsed);
                        dailyShed.setShed_ID(shed.getId());

                        methods.saveDailyShed(dailyShed);
                        //Setting the inputs back to nothing for the user
                        mortUpdate.setText("");
                        feedUpdate.setText("");

                    }

                } else {
                    //If run has not been started, show Toast
                    Toast.makeText(ShedDetailActivity.this, "Need to Start Run First", Toast.LENGTH_LONG).show();
                }
            }
        });


    }

    //Class to compare the time from the previous time
    public class TimeIgnoringComparator implements Comparator<Date> {
        public int compare(Date d1, Date d2) {
            if (d1.getYear() != d2.getYear())
                return d1.getYear() - d2.getYear();
            if (d1.getMonth() != d2.getMonth())
                return d1.getMonth() - d2.getMonth();
            return d1.getDate() - d2.getDate();
        }
    }

    //Getting the date
    private Date getDate(String dateString) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date = format.parse(dateString);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    //Validation for Feed Input
    private boolean updateFeedValidation() {
        boolean isValid = true;

        if (input.getText().toString().trim().isEmpty()) {
            inputLayout.setError(getString(R.string.msg_cannot_empty));
            isValid = false;
        } else {
            inputLayout.setError(null);
        }
        return isValid;
    }


    @Override
    //Option Menu Creation - this is for Start Run and End Run
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.shed_action, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_startRun) {
            //if Start Run has already been used do nothing, otherwise showDialog and set Start Run to 1
            if (shed.getStartRun() == 0) {
                shed.setStartRun(1);
                showDialog();
            }
            return true;
        } else if (id == R.id.action_endRun) {
            if (shed.getStartRun() == 0) {//Need to Start Run before ending it
                Toast.makeText(ShedDetailActivity.this, "Need to start run before you can end it", Toast.LENGTH_LONG).show();
            } else {//Calculate Corrected FCR Result
                calculateCorrectedFCR();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void calculateCorrectedFCR() {
        //This calculation corrects the liveweight back to 2.3kg to make the result far across the board for the pool system the farmer will be in
        //As birds will be taken at different ages to be processed this means they wll be at different weights, setting them back to 2.3 corrects this
        final AlertDialog.Builder builder = new AlertDialog.Builder(ShedDetailActivity.this);
        builder.setTitle("Enter Figures to Calculate Corrected FCR ");
        builder.setCancelable(false);

        //Inflating the custom view
        View viewInflated = LayoutInflater.from(ShedDetailActivity.this).inflate(R.layout.layout_fcr_dialog, null);

        // Set up the input
        input = viewInflated.findViewById(R.id.edtTotalLiveWeight);
        input2 = viewInflated.findViewById(R.id.edtNumberOfProcessed);
        input3 = viewInflated.findViewById(R.id.edtavgWeight);

        inputLayout = viewInflated.findViewById(R.id.tilTotalLiveWeightWrapper);
        inputLayout2 = viewInflated.findViewById(R.id.tilNumberOfProcessedWrapper);
        inputLayout3 = viewInflated.findViewById(R.id.tilavgWeightWrapper);

        // Specify the type of input expected; this
        builder.setView(viewInflated);

        // Set up the buttons
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (validation3()) {
                    methods = new DB_helper_methods(ShedDetailActivity.this);
                    List<DailyShed> list = methods.getDailyShed(shed.getId());

                    if (list.size() > 0) {
                        double totalFeedUsed = 0.0;

                        for (DailyShed dailyShed : list) {
                            totalFeedUsed += dailyShed.getFeed_Count_Shed();
                        }

                        double demon = shed.getNum_birds() * Double.valueOf(input.getText().toString());
                        if (demon > 0.0) {
                            double FCR = totalFeedUsed / demon;
                            if (Long.valueOf(input2.getText().toString()) > 0) {//Calculation using the inputs from the user
                                Double liveWeight = Double.valueOf(input.getText().toString());
                                Long birdProcessed = Long.valueOf((input2.getText().toString()));
                                Double finalCalc = 2300 - (liveWeight / birdProcessed);
                                finalCalc = (finalCalc / 100) * (0.0190 + FCR);


                                dialog.dismiss();

                                final AlertDialog.Builder builder1 = new AlertDialog.Builder(ShedDetailActivity.this);
                                builder1.setTitle("Here is your Corrected FCR Result");
                                builder1.setMessage("CFCR:" + finalCalc);
                                builder1.setCancelable(false);
                                builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        //Reset everything back to Zero or null for the next Run

                                        shed.setStartRun(0);
                                        shed.setDayNumber(0);
                                        shed.setNum_birds(0);
                                        shed.setFeed_count(0.0);
                                        shed.setDate(null);
                                        methods.updateShed(shed);
                                        methods.deleteDailyShed(shed.getId(), ShedDetailActivity.this);

                                        //Update the UI
                                        numberOfDayCount.setText("0");
                                        numberOfChickens.setText("0");
                                        feedLeft.setText("0.0");
                                        mortUpdate.setText("");
                                        feedUpdate.setText("");
                                        mortUpdate.setFocusable(false);
                                        feedUpdate.setFocusable(false);


                                    }
                                });
                                builder1.show();
                            }
                        }
                    }

                }
            }


        });


        builder.show();
    }

    private void showDialog() {
        //Dialog for Starting Run
        final AlertDialog.Builder builder = new AlertDialog.Builder(ShedDetailActivity.this);
        builder.setTitle("Start New Run ");
        builder.setCancelable(false);

        //Inflating the custom view
        View viewInflated = LayoutInflater.from(ShedDetailActivity.this).inflate(R.layout.layout_dialog, null);

        // Set up the input
        input = viewInflated.findViewById(R.id.edtStartChicken);
        input2 = viewInflated.findViewById(R.id.edtStartFeed);
        inputLayout = viewInflated.findViewById(R.id.tilstartChickenWrapper);
        inputLayout2 = viewInflated.findViewById(R.id.tilstartFeedWrapper);
        input.setHint("Enter Starting Number of Chickens");
        // Specify the type of input expected; this
        builder.setView(viewInflated);

        // Set up the buttons
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (validation()) {

                    mortUpdate.setFocusableInTouchMode(true);
                    feedUpdate.setFocusableInTouchMode(true);
                    methods = new DB_helper_methods(ShedDetailActivity.this);
                    //Setting date of creation
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String strDate = sdf.format(new Date());
                    //Setting values
                    shed.setDate(strDate);
                    shed.setNum_birds(Long.valueOf(input.getText().toString()));
                    shed.setFeed_count(Double.valueOf(input2.getText().toString()));
                    numberOfChickens.setText(String.valueOf(shed.getNum_birds()));
                    feedLeft.setText("Feed Left: " + String.valueOf(shed.getFeed_count()) + " Tonne");
                    shed.setStartRun(1);
                    methods.updateShed(shed);
                    dialog.dismiss();
                }
            }


        });


        builder.show();

    }

    //Three Validations for different stages. Most of them is checking if empty
    private boolean validation() {

        boolean isValid = true;

        if (input.getText().toString().trim().isEmpty()) {
            inputLayout.setError("Cannot be Empty");
            isValid = false;
        } else {
            inputLayout.setError(null);
        }

        if (input2.getText().toString().trim().isEmpty()) {
            inputLayout2.setError("Cannot be Empty");
            isValid = false;
        } else {
            inputLayout2.setError(null);
        }
        return isValid;
    }

    private boolean validation2() {

        boolean isVaild = true;

        if (feedUpdate.getText().toString().isEmpty()) {
            feedUpdateWrapper.setError("Cannot be Empty");
            return false;
        } else if (shed.getNum_birds() != 0 && (Double.valueOf(feedUpdate.getText().toString()) * shed.getNum_birds()) > shed.getFeed_count()) {
            //Because there cannot be less feed in the silos than the birds have eaten, the user will either need to correct the values or add more feed
            feedUpdateWrapper.setError("Need more Feed in Silo");
            return false;

        } else {
            isVaild = true;
            feedUpdateWrapper.setError(null);
        }

        if (mortUpdate.getText().toString().isEmpty()) {
            isVaild = false;
            mortUpdateWrapper.setError(("Cannot be empty"));
        } else if (shed.getNum_birds() < Long.valueOf(mortUpdate.getText().toString())) {

            isVaild = false;
            //Cannot have more mortality's than there are alive chickens
            mortUpdateWrapper.setError("Value cannot be greater than total number of birds");
        } else {
            isVaild = true;
            mortUpdateWrapper.setError(null);
        }

        return isVaild;
    }

    private boolean validation3() {
        boolean isValid = true;
        if (input.getText().toString().trim().isEmpty()) {
            inputLayout.setError(getString(R.string.msg_cannot_empty));
            isValid = false;
        } else {
            inputLayout.setError(null);
        }

        if (input2.getText().toString().trim().isEmpty()) {
            inputLayout2.setError(getString(R.string.msg_cannot_empty));
            isValid = false;
        } else {
            inputLayout2.setError(null);
        }
        if (input3.getText().toString().trim().isEmpty()) {
            inputLayout3.setError(getString(R.string.msg_cannot_empty));
            isValid = false;
        } else {
            inputLayout3.setError(null);
        }
        return isValid;
    }
}

